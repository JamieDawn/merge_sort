﻿def merge_sort(n):
    if len(n) <= 1:
        return n
    partA = n[:len(n)//2]
    partB = n[len(n)//2:]
    left = merge_sort(partA)
    right = merge_sort(partB)
    return reduce(left, right)

def reduce(left, right):
    l = 0
    r = 0
    newlist = []
    while l < len(left) and r < len(right):
        if left[l] < right[r]:
            newlist.append(left[l])
            l += 1
        else:
            newlist.append(right[r])
            r += 1
    if l == len(left):
        newlist.extend(right[r:])
    if r == len(right):
        newlist.extend(left[l:])
    return newlist