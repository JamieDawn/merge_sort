﻿import random
import time
import matplotlib.pyplot as plt
import merge_sort

list_length = [500, 1000, 2000, 4000, 8000, 16000, 32000, 64000]
best_time = []
time_ = []

#best-case scenario
for length in list_length:
    best_list = list(range(0, length + 1))
    start = time.time()
    merge_sort.merge_sort(best_list)
    end = time.time()
    best_time.append(end-start)
    print("The time for sorting a list of %s numbers is %s second(s)."%(length,(end-start)))

plt.plot(list_length, best_time, label = 'best')


#worst- and average-case scenarios
for length in list_length:
    list_ = list(range(0, length + 1))
    random.shuffle(list_)
    start = time.time()
    merge_sort.merge_sort(list_)
    end = time.time()
    time_.append(end-start)
    
plt.plot(list_length, time_, label = 'worst and average')
plt.legend(['Best-case', 'Worst- and average-case'])
plt.show()